#include <iostream>
#include <string>
using namespace std;

// create the class animal
class Animal{
public:
    Animal(string species, int weight, string sound) : // chatacteristics of the class
    species(species), weight(weight), sound(sound){

    }
    // return the species of the animal
    string getSpecies(){ 
        return species;
    }
    // return the sound the animal makes
    string getSound(){
        return sound;
    }
    // return the weight of the animal
    int getWeight(){
        return weight;
    }
    // prints the species, weight and noise the animal makes
    void print(){
        cout << species << " " <<  weight << "kg " << sound << endl;
    }
// protected means theyre accessable by derived classes but cannot be changed
protected:
    string species;
    string sound;
    int weight; 
};

class Cow : public Animal{
public:
    Cow(string species, int weight, string sound, int noOfSpots) : // added a characteristc, no of spots
    spots(noOfSpots),
    Animal(species, weight, sound){

    }
    // return the no of spots
    int getSpots(){
        return spots;
    }
    // prints all the charactreristics of a cow
    void print(){
        cout << species << " " <<  weight << "kg " << sound << " " << spots << endl;
    }


protected:
    int spots;
};

class Chicken : public Animal{
public:
    Chicken(string species, int weight, string sound, int noOfEggs) : // addes a characteristic, no of eggs
    eggs(noOfEggs),  
    Animal(species, weight, sound){

    }
    // return no of eggs
    int getEggs(){
        return eggs;
    }

    void print(){
        cout << species << " " <<  weight << "kg " << sound << " " << eggs << endl;
    }


protected:
    int eggs;
};