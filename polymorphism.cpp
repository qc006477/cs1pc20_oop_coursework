#include <iostream>
#include "polymorphism.hpp"
using namespace std;

int main(){
    // creates an animal called oscar who is a dog, weighs 15 kg, and goes woof
    Animal Oscar("Dog", 15, "woof");
    // prints the details entered above
    Oscar.print();
    // creates a cow called eve who weighs 700kg, goes moo and has 20 spots
    Cow Eve("Cow", 700, "moo", 20);
    Eve.print();
    // creates a chicken called rupert who weighs 5kg, goes cluck and has layed 2 eggs
    Chicken Rupert("Chicken", 5, "cluck", 2);
    Rupert.print();

    return 0;
}